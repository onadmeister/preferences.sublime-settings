This is my Sublime Text 3 settings, I personally use ST3 as my main text editor for coding. Feel free to mix n match with your own.


### List of Packages ###

* [Package Control](https://packagecontrol.io/)
* [Boxy Theme](https://github.com/drosen0/boxy)
* [Hack Font](https://github.com/source-foundry/Hack)
* [Laravel Blade Highlighter](https://packagecontrol.io/packages/Laravel%20Blade%20Highlighter)
* [Laravel Blade Spacer](https://packagecontrol.io/packages/Laravel%20Blade%20Spacer)